from app import db


class User(db.Model):
  """todo: write docstring for this class"""
  email = db.Column('email', db.String, primary_key=True)
  name = db.Column('name', db.String)
  password = db.Column('password', db.String)

  def __init__(self,email,name,password):
    self.email = email
    self.name = name
    self.password = password


class functions:

  def addNewUser(email,name,password):
    new_user = User(email,name,password)
    db.session.add(new_user)
    db.session.commit()

  def userExists(email):
    test = User.query.filter(User.email == email).first()
    if test is not None:
      return True
    else:
      return False

  def findPassword(email):
    test = User.query.filter(User.email == email).first()      
    return (test.password)
    
  def findName(email):
    test = User.query.filter(User.email == email).first()      
    return (test.name)

#todo: put these functions into a class?
# def addNewUser(email,name,password):
#   new_ex = User(email,name,password)
#   db.session.add(new_ex)
#   db.session.commit()

# def userExists(email):
#   test = User.query.filter(User.email == email).first()
#   if test is not None:
#     return True
#   else:
#     return False

# def findPassword(email):
#   test = User.query.filter(User.email == email).first()      
#   return (test.password)
# def findName(email):
#   test = User.query.filter(User.email == email).first()      
#   return (test.name)


