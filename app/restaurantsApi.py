
#!/usr/bin/env python3
 
import json, requests
 
latitude = -33.938007 #-33.8670522
longitude = 151.241168 #151.1957362
defaultRadius = 500
defaultType = 'restaurant'
key = 'AIzaSyCgg6AEmeAGsMOwB7Z4NhxBQUel5_3rgPw' # Irfan's key

def getPlaces(latitude, longitude, radius = defaultRadius, type = defaultType, maxResults=10, maxWidth=1600, defaultImageUrl=''):
  url = ('https://maps.googleapis.com/maps/api/place/nearbysearch/json?' 
        + 'location=' + str(latitude) + ',' + str(longitude) + '&'
        + 'radius=' + str(radius) + '&'
        + 'type=' + type + '&'
        + 'key=' + key)
 
  r = requests.get(url)
  data = json.loads(r.text)['results']
  output = []
 
  for i in range(0, min(maxResults, len(data))):
    place = {}
    try:
      place['id'] = data[i]['place_id']
      place['name'] = data[i]['name']
      place['rating'] = data[i]['rating']
      place['vicinity'] = data[i]['vicinity'] # This is the distance
      place['photoAttribution'] = data[i]['photos'][0]['html_attributions'][0] # This must be displayed with photo (see T&C)*
      place['photoUrl'] = ('https://maps.googleapis.com/maps/api/place/photo?'
                               + 'maxwidth=' + str(maxWidth) + '&'
                               + 'photoreference=' + data[i]['photos'][0]['photo_reference'] + '&'
                               + 'key=' + str(key))
      output.append(place)
 
    # Not all of the above fields may be present. Skip these for now.
    except KeyError:
      pass
    except IndexError:
      pass
 
  return json.dumps(output)
 
if __name__ == '__main__':
  try:
    print(json.loads(getPlaces(latitude, longitude, defaultRadius, defaultType))[0])
  except TypeError:
    # getPlaces() returned None
    print("Some data is not available")
 
'''
*if the returned photo element includes a value in the html_attributions field,
you will have to include the additional attribution in your application
wherever you display the image.
(https://developers.google.com/places/web-service/photos)
 
SAMPLE OF JSON OUPUT
r.text() =
{
  "html_attributions" : [],
  "next_page_token" : "CqQCGwEAACnl1ZN-92ZuqdnBa65d50-YBCJsFk9tjzrLJ7Iefk0LcWdGnDhzHhSz4vBcwYeYgy2kJDOoczupNjT0ssrCV81sqvzsW4J3qFNBOAZ3oD5Y5-PCHrrwkRVk3F7eI4wKl8ExO35pNOjHqTqUrSk3ERgPzwonOC1g4cjVLkKh34FvJrcAg6G2YXK8jHwwJcS6Zoxxv6Uf_52bM0A6S-fiDLB0t3nl7tsQhSwukfXrfdHOk4oaq017PnHwfI9B1He7SUwDIgr_7J20Qhk2lroaOLA6zwVeT20DI6cpVHiGDryfXC_mfLQ5TwXZa6LXDnEeYs5ozgIXnpEcPJfaYUB1V_DplWQi_H8J3FYdrpOWgQDf99RueHWixbBdpagZklzz5BIQ5qFl7wGxOSsn8jB74Ygs6hoU0bOkbb0fiUMrmGD-P2YBlGmNWNU",
  "results" : [
     {
        "geometry" : {
           "location" : {
              "lat" : -33.87036190000001,
              "lng" : 151.1978505
           },
           "viewport" : {
              "northeast" : {
                 "lat" : -33.8690182697085,
                 "lng" : 151.1991515302915
              },
              "southwest" : {
                 "lat" : -33.8717162302915,
                 "lng" : 151.1964535697085
              }
           }
        },
        "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
        "id" : "e58f0f9ecaf15ab719d305b93265cafc00b01a3f",
        "name" : "The Little Snail Restaurant",
        "opening_hours" : {
           "open_now" : false,
           "weekday_text" : []
        },
        "photos" : [
           {
              "height" : 585,
              "html_attributions" : [
                 "\u003ca href=\"https://maps.google.com/maps/contrib/114727320476039103791/photos\"\u003eThe Little Snail Restaurant\u003c/a\u003e"
              ],
              "photo_reference" : "CmRaAAAAEETTjTJgy_nDEHBHGrb-9KJti7FIGxWfuX_eKhyNUKDCiVatgJyDhURjQR_6xCTf9ISzTk_h42rM_M60nRxadw7RtNeUA3ZZfnMsS-6lnBeYYaM-_nkP3lP9Dm4STuddEhCBkM565Q8K-arxjQgWQQjsGhQb4h0NXXfgiUnsj8L6VCNgupiceQ",
              "width" : 582
           }
        ],
        "place_id" : "ChIJtwapWjeuEmsRcxV5JARHpSk",
        "price_level" : 2,
        "rating" : 4.2,
        "reference" : "CmRRAAAAje_oOFn98MN17xfO4yr9S_JLYmNqBZVfHVyKkG3xVtor6cJPddksg-p2Q0fAZrqzOT-OBzOkVGH1kyjkYoe_MlmRvbDgcoGtorlAevI0f9ddFntUf2FfX8XoRbfPh7H3EhB0xdiQm0_yTqvZGbWgZOAwGhQs_TBf2m7nJ7n_l3q76gQplU56FA",
        "scope" : "GOOGLE",
        "types" : [ "restaurant", "food", "point_of_interest", "establishment" ],
        "vicinity" : "3/50 Murray Street, Pyrmont"
     }
  ],
  "status" : "OK"
}
'''