import unittest
import os
from app import models
from app import db
from app import app
from flask import Flask

TEST_DB = 'unittest.db'

class DBTests(unittest.TestCase):
  """simple test to test basic server functionality"""
  def setUp(self):

    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['DEBUG'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
    self.app = app.test_client()
    db.drop_all()
    db.create_all()

    models.functions.addNewUser("test@test.com", "abc", "123")

  def tearDown(self):
    db.drop_all()
    
  def test_userExists(self):    
    self.assertEqual(True, models.functions.userExists("test@test.com"))

  def test_findPassword(self):    
    self.assertEqual("123", models.functions.findPassword("test@test.com"))

  def test_findName(self):    
    self.assertEqual("abc", models.functions.findName("test@test.com"))
 
if __name__ == '__main__':
  unittest.main()
  

