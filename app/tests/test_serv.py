from app import app
import unittest
import os
import json

class SimpleTest(unittest.TestCase):
  """simple test to test basic server functionality"""
  def setUp(self):
    """sets up the test class for the server"""
    #create a test client
    self.app = app.test_client()
    #propagate exceptions
    self.app.testing = True
  
  def tearDown(self):
    pass
    
  def test_home_status_code(self):
    """tests the status code of the root"""
    # seng GET request to server
    result = self.app.get('/')

    # test status code
    self.assertEqual(result.status_code, 200)

  def test_home_data(self):
    """tests that an unlogged in session returns a not logged in message """
    result = self.app.get('/')
    self.assertEqual(result.data, b'you are not logged in\n')

class getPlacesTest(unittest.TestCase):
  """simple test to test basic server functionality"""
  def setUp(self):
    """sets up the test class for the server"""
    #create a test client
    self.app = app.test_client()
    #propagate exceptions
    self.app.testing = True
  
  def tearDown(self):
    pass

  def test_garbageURL(self):
    garbage = '?' + str(os.urandom(64))

    result = self.app.get('/getplaces'+garbage)
    self.assertEqual(result.status_code, 400) #bad request
  
  def test_returnType(self):
    lati = str(-33.938007) #-33.8670522
    longi = str(151.241168) #151.1957362

    result = self.app.get('/getplaces?latitude='+lati+'&longitude='+longi)
    raised = False
    try:
      json.loads(result.data.decode("utf-8"))
    except ValueError:
      raised = True
    self.assertFalse(raised)
    self.assertEqual(result.status_code, 200)



    

if __name__ == '__main__':
  unittest.main()
  