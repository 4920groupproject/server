from flask import Flask, request, session, escape, send_from_directory, url_for, redirect
from app import app, models, restaurantsApi
import json
import os
 
@app.route('/')
def root():
  if 'email' in session:
    name___ = models.functions.findName(session['email'])
    return 'Loged in as {}'.format(escape(name___))
  return 'you are not logged in\n'

@app.route('/favicon.ico')
def favicon():
  return send_from_directory(os.path.join(app.root_path, 'static'),
                             'favicon.ico', mimetype ='image/vnd.microsoft.icon')
 
@app.route("/login", methods=['POST', 'GET'])
def login():
  if request.method == 'POST':
   
    if not models.functions.userExists(request.form['email']):
      return 'Does not exist'
    if models.functions.findPassword(request.form['email']) != request.form['password']:
      return 'Wrong password'
   
    session['email'] = request.form['email']
 
    print("go to root to verify username is: {}".format(request.form['email']))
    return redirect(url_for('root'))
  return '''
       <form method="post">
           <p>Email:    <input type=text name=email>
           <p>Password: <input type=text name=password>
           <p>          <input type=submit value=Login>
       </form>
   '''
 
@app.route("/signup", methods=['POST', 'GET'])
def signup():
  if request.method == 'POST':
    e = request.form['email']
    n = request.form['username']
    p = request.form['password']
 
    if models.functions.userExists(e):
      return "exists!"
    models.functions.addNewUser(e,n,p)
   
    session['email'] = e
    print("go to root to verify username is: {}".format(request.form['email']))
    return redirect(url_for('root'))
  return '''
       <form method="post">
           <p>Email:    <input type=text name=email>
           <p>Username: <input type=text name=username>
           <p>Password: <input type=text name=password>
           <p>          <input type=submit value=Signup/Login>
       </form>
   '''
 
 
@app.route("/login/form")
def form():
  return send_from_directory('pages', 'login.html')
 
@app.route("/logout")
def logout():
  session.pop('email', None)
  print("you have been logged out\n")
  return redirect(url_for('root'))
 
@app.route('/json')
def sendJSON():
  d = {"first": "rob", "last":"burke", "age": 54}
  return json.dumps(d) + "\n"

@app.route("/getplaces", methods=['GET', 'POST'])
def getPlaces():
  # accept GET or POST
  if request.method == 'GET':
    latArg = request.args.get('latitude')
    longArg = request.args.get('longitude')
  elif request.method == 'POST':
    latArg = request.form['latitude']
    longArg = request.form['longitude']
  else:
    return "Error: request Invalid\nNeither GET or POST", 400

  # convert to float so that an error is thrown if bad data
  try:
    lati = float(latArg)
    longi = float(longArg)
  except TypeError as err:
    # print(err) #for debugging
    pass
    return "Error: request Invalid", 400

  return restaurantsApi.getPlaces(lati, longi)

app.secret_key = b'0G0\x7f\xb9}?\xcd\xf5|\xbb\xef\x7f\xfe!\x17\rHj\xa6\xe2 +\x1c'