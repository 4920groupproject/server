from flask import Flask, request, session, escape, send_from_directory, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
import json
import os
 
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///testdb.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class User(db.Model):
  email = db.Column('email', db.String, primary_key=True)
  name = db.Column('name', db.String)
  password = db.Column('password', db.String)

  def __init__(self,email,name,password):
    self.email = email
    self.name = name
    self.password = password

def addNewUser(email,name,password):
  new_ex = User(email,name,password)
  db.session.add(new_ex)
  db.session.commit()

def Exist(email):
  test = User.query.filter(User.email == email).first()
  if test is not None:
    return True
  else:
    return False

def findPassword(email):
  test = User.query.filter(User.email == email).first()      
  return (test.password)
def findName(email):
  test = User.query.filter(User.email == email).first()      
  return (test.name)
################################
@app.route('/')
def root():
  print(session)
  print(app.secret_key)
  if 'email' in session:
    name___ = findName(session['email'])
    return 'Loged in as {}'.format(escape(name___))
  return 'you are not logged in\n'

with app.test_request_context("/json"):
  print(request.path)
  assert request.path == "/json"

@app.route("/login", methods=['POST', 'GET'])
def login():
  if request.method == 'POST':
    
    if Exist(request.form['email']) == False:
      return 'Does not exist'
    if findPassword(request.form['email']) != request.form['password']: 
      return 'Wrong password'
     
    session['email'] = request.form['email']

    print("go to root to verify username is: {}".format(request.form['email']))
    return redirect(url_for('root'))
  return '''
        <form method="post">
            <p><input type=text name=email>
            <p><input type=text name=password>
            <p><input type=submit value=Login>
        </form>
    '''

@app.route("/signup", methods=['POST', 'GET'])
def signup():
  if request.method == 'POST':
    e = request.form['email']
    n = request.form['username']
    p = request.form['password']

    if Exist(e) == True:
      return "exists!"
    addNewUser(e,n,p)
    
    session['email'] = e
    print("go to root to verify username is: {}".format(request.form['email']))
    return redirect(url_for('root'))
  return '''
        <form method="post">
            <p><input type=text name=email>
            <p><input type=text name=username>
            <p><input type=text name=password>
            <p><input type=submit value=Signup/Login>
        </form>
    '''


@app.route("/login/form")
def form():
  return send_from_directory('pages', 'login.html')

@app.route("/logout")
def logout():
  session.pop('username', None)
  print("you have been logged out\n")
  return redirect(url_for('root'))

@app.route('/var/<int:num>')
def pinnum(num):
  return "Number is {}".format(num)

@app.route('/json')
def sendJSON():
  d = {"first": "rob", "last":"burke", "age": 54}
  return json.dumps(d) + "\n"

app.secret_key = os.urandom(24) #todo make static
#use 
# with app.test_request_context():
#   url_for("some_function")

